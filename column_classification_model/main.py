from column_model_code.src.process_column_iden import get_column_header_initial
from column_model_code.src.rotate_by_angle import rotate_images, resize_images
import sys
import configparser
import os
import shutil
import random
import glob
import pdf2image
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
import keras
sys.stderr = stderr
import keras.backend as K
from keras.models import load_model


def gen_images_dir(pdf_path, page_number, output_path, output_dir_capacity, temp_dpi):
    pdf_name = pdf_path[(pdf_path.rfind('/') + 1):]
    pdf_out_path = output_path + pdf_name[:-4] + '/'

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    output_path_content = os.listdir(output_path)

    if len(output_path_content) > output_dir_capacity and pdf_name[:-4] not in output_path_content:
        random.shuffle(output_path_content)
        shutil.rmtree(output_path_content + output_path_content[0])

    if pdf_name[:-4] in output_path_content:
        shutil.rmtree(pdf_out_path)

    os.makedirs(pdf_out_path)
    try:
        for page in page_number:
            temp_image = pdf2image.convert_from_path(pdf_path, dpi=temp_dpi, first_page=page, last_page=page, fmt='jpg')
            temp_img_path = pdf_out_path + pdf_name[:-4] + '_Page' + str(page) + 'Temp.jpg'
            temp_image[0].save(temp_img_path)
    except Exception:
        print('PDF Error')
    return pdf_out_path


def get_column_model_output(input_dict):
    page_number = []
    pdf_path = ''

    config = configparser.ConfigParser()
    config.read('./column_model_code/config.ini')
    output_path = config['COLUMN_DETECTION']['output_path']
    output_dir_capacity = int(config['COLUMN_DETECTION']['output_dir_capacity'])
    temp_dpi = int(config['COLUMN_DETECTION']['temp_dpi'])
    column_detection_model_path = config['COLUMN_DETECTION']['model_path']

    column_detection_model = load_model(column_detection_model_path, compile=False)

    for key, val in input_dict['data'].items():
        if key == 'BS' or key == 'IS' or key == 'CF':
            for page in val:
                page_number.append(page)

    pdf_path = input_dict['data']['pdf_path']
    pdf_out_path = gen_images_dir(pdf_path, page_number, output_path, output_dir_capacity, temp_dpi)

    image_path_list = glob.glob(pdf_out_path + '*Temp.jpg')

    rotate_images(image_path_list, input_dict)
    resize_images(image_path_list, input_dict)

    for image_path in image_path_list:
        for each_dtls in input_dict['data']['table_cell_column']:
            column_coord, input_image_path = each_dtls[2], each_dtls[3]
            filename = input_image_path[(input_image_path.rfind('/') + 1):]
            if filename in image_path:
                each_dtls[2] = get_column_header_initial(image_path, column_detection_model, column_coord)
    return input_dict
