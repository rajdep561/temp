import math
import cv2
import numpy as np


def rotate_image(image, angle):
    """
    Rotates an image about it's centre by the given angle. Image will be
    large enough to hold the entire new image, with a black background
    :param image: Original Image to be rotated
    :param angle: Angle
    :return: Rotated Image
    """
    image_size = (image.shape[1], image.shape[0])
    image_center = tuple(np.array(image_size) / 2)
    rot_mat = np.vstack([cv2.getRotationMatrix2D(image_center, angle, 1.0), [0, 0, 1]])
    rot_mat_notranslate = np.matrix(rot_mat[0:2, 0:2])
    image_w2 = image_size[0] * 0.5
    image_h2 = image_size[1] * 0.5
    rotated_coords = [
        (np.array([-image_w2, image_h2]) * rot_mat_notranslate).A[0],
        (np.array([image_w2, image_h2]) * rot_mat_notranslate).A[0],
        (np.array([-image_w2, -image_h2]) * rot_mat_notranslate).A[0],
        (np.array([image_w2, -image_h2]) * rot_mat_notranslate).A[0]
    ]
    x_coords = [pt[0] for pt in rotated_coords]
    x_pos = [x for x in x_coords if x > 0]
    x_neg = [x for x in x_coords if x < 0]
    y_coords = [pt[1] for pt in rotated_coords]
    y_pos = [y for y in y_coords if y > 0]
    y_neg = [y for y in y_coords if y < 0]

    right_bound = max(x_pos)
    left_bound = min(x_neg)
    top_bound = max(y_pos)
    bot_bound = min(y_neg)
    new_w = int(abs(right_bound - left_bound))
    new_h = int(abs(top_bound - bot_bound))

    trans_mat = np.matrix([
        [1, 0, int(new_w * 0.5 - image_w2)],
        [0, 1, int(new_h * 0.5 - image_h2)],
        [0, 0, 1]
    ])
    affine_mat = (np.matrix(trans_mat) * np.matrix(rot_mat))[0:2, :]
    result = cv2.warpAffine(
        image,
        affine_mat,
        (new_w, new_h),
        flags=cv2.INTER_LINEAR
    )
    return result


def largest_rotated_rect(w, h, angle):
    """
    Computes the width and height of the largest possible axis-aligned
    rectangle within the rotated image.
    :param w: width of the image
    :param h: height of the image
    :param angle: angle at which image is rotated
    :return: width, height of largest rectangle
    """
    quadrant = int(math.floor(angle / (math.pi / 2))) & 3
    sign_alpha = angle if ((quadrant & 1) == 0) else math.pi - angle
    alpha = (sign_alpha % math.pi + math.pi) % math.pi
    bb_w = w * math.cos(alpha) + h * math.sin(alpha)
    bb_h = w * math.sin(alpha) + h * math.cos(alpha)
    gamma = math.atan2(bb_w, bb_w) if (w < h) else math.atan2(bb_w, bb_w)
    delta = math.pi - alpha - gamma
    length = h if (w < h) else w
    d = length * math.cos(alpha)
    a = d * math.sin(alpha) / math.sin(delta)
    y = a * math.cos(gamma)
    x = y * math.tan(gamma)
    return bb_w - 2 * x, bb_h - 2 * y


def crop_around_center(image, width, height):
    """
    Given an image, crops it to the given width and height, around its
    center point
    :param image: numpy image
    :param width: width of image
    :param height: height of image
    :return: cropped image
    """
    image_size = (image.shape[1], image.shape[0])
    image_center = (int(image_size[0] * 0.5), int(image_size[1] * 0.5))
    if width > image_size[0]:
        width = image_size[0]
    if height > image_size[1]:
        height = image_size[1]
    x1 = int(image_center[0] - width * 0.5)
    x2 = int(image_center[0] + width * 0.5)
    y1 = int(image_center[1] - height * 0.5)
    y2 = int(image_center[1] + height * 0.5)
    return image[y1:y2, x1:x2]


def rotate_by_angle(image, angle):
    """
    Rotates an image by a given angle cropping out the borders
    :param image: numpy image
    :param angle: angle at which image needs to be rotated
    :return: rotated image
    """
    image_height, image_width = image.shape[0:2]
    image_rotated = rotate_image(image, angle)
    image_rotated_cropped = crop_around_center(image_rotated, *largest_rotated_rect(image_width, image_height, math.radians(angle)))
    return image_rotated_cropped


def rotate_images(images_path_list, rotation_output):
    for image_path in images_path_list:
        page_number = int(image_path[image_path.rfind(
            'Page') + 4: image_path.rfind('Temp.jpg')])
        if page_number in rotation_output['data']['BS']:
            for img_dtls in rotation_output['data']['BS_rotation']:
                if page_number == img_dtls[0]:
                    angle = img_dtls[1]
                    image_low = cv2.imread(image_path)
                    rot_image1 = rotate_by_angle(image_low, angle)
                    cv2.imwrite(image_path, rot_image1)
        if page_number in rotation_output['data']['IS']:
            for img_dtls in rotation_output['data']['IS_rotation']:
                if page_number == img_dtls[0]:
                    angle = img_dtls[1]
                    image_low = cv2.imread(image_path)
                    rot_image1 = rotate_by_angle(image_low, angle)
                    cv2.imwrite(image_path, rot_image1)
        if page_number in rotation_output['data']['CF']:
            for img_dtls in rotation_output['data']['CF_rotation']:
                if page_number == img_dtls[0]:
                    angle = img_dtls[1]
                    image_low = cv2.imread(image_path)
                    rot_image1 = rotate_by_angle(image_low, angle)
                    cv2.imwrite(image_path, rot_image1)


def resize_images(image_path_list, input_dict):
    for image_path in image_path_list:
        for each_dtls in input_dict['data']['table_cell_column']:
            input_image_path = each_dtls[3]
            filename = input_image_path[(input_image_path.rfind('/') + 1):]
            if filename in image_path:
                w, h = each_dtls[4][0], each_dtls[4][1]
                img_read = cv2.imread(image_path)
                img_read = cv2.resize(
                    img_read, (w, h), interpolation=cv2.INTER_AREA)
                cv2.imwrite(image_path, img_read)
