from keras.applications.mobilenet_v2 import preprocess_input
from PIL import Image
import numpy as np
import configparser
import cv2

config = configparser.ConfigParser()
config.read('./column_model_code/config.ini')

img_width = int(config['COLUMN_DETECTION']['img_width'])
img_height = int(config['COLUMN_DETECTION']['img_height'])
column_identification_score = float(config['COLUMN_DETECTION']['threshold_score'])


def load_image(img_path):
    width, height = (img_width, img_height)
    img = Image.fromarray(img_path).resize((width, height))
    img_np = np.asarray(img)
    img_tensor = preprocess_input(np.expand_dims(img_np, axis=0))
    return img_tensor


def batch_predict(files, model):
    width, height = (img_width, img_height)
    img = np.zeros((len(files), height, width, 3))
    for j in range(img.shape[0]):
        img[j, :] = load_image(files[j])[0]
    predictions = model.predict(img)
    file_pred_list = []
    for i, prediction in enumerate(predictions):
        match = []
        page_class = []
        for idx in range((prediction > column_identification_score).shape[0]):
            if (prediction > column_identification_score)[idx]:
                match.append(idx)
        if len(match) < 1:
            page_class.append('Others')
        else:
            for match in match:
                if match == 0:
                    page_class.append('Amount')
                elif match == 1:
                    page_class.append('Notes')
                elif match == 2:
                    page_class.append('Particulars')
        file_pred_list.append(page_class)
    return file_pred_list


def get_column_header_initial(image_path, column_detection_model, column_coord):
    image_cv = cv2.imread(image_path)
    column_crops, column_crops_coord = [], []
    for column in column_coord:
        column_crops.append(image_cv[column[1]:column[3], column[0]:column[2]])
        column_crops_coord.append([column[0], column[1], column[2], column[3]])
    column_pred = batch_predict(column_crops, column_detection_model)
    for i in range(len(column_crops_coord)):
        try:
            prediction = str(column_pred[i])
        except Exception:
            prediction = ''
        column_crops_coord[i].append(prediction)
    return column_crops_coord
